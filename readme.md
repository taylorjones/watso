# Pi Cluster

This application provides a web interface for users to submit mpi jobs to Dr. Watson's raspberry pi cluster.

## Dependencies

Most dependencies are handled by Node Package Manager (just run `npm install`).
However, this program also relies on a python script to watch for jobs and run them in order.
This script requires pymongo. To install pymongo run `pip install pymongo`.

In order to properly function as a cluster, the master pi (pi 0) must host an NFS share for the other nodes to have access to the executable.
The recommended folder to share is `/home/pi/watso/jobs`. For instructions on setting up an NFS share, see https://www.ev3dev.org/docs/tutorials/setting-up-an-nfs-file-share/.

Additionally, each pi in the cluster must share an ssh key with every other pi in the cluster in order for MPI to communicate between nodes over the network. 
See [setup.sh](https://gitlab.com/taylorjones/watso/blob/master/setup.sh).

It is recommended that each pi be running the same version of [raspbian-lite](https://www.raspberrypi.org/downloads/raspbian/).
