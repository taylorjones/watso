var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var ClassSchema = new mongoose.Schema({
  classname: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  classid: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  allowregister: {
    type: Boolean,
    required: true,
  }
});

//authenticate input against database
ClassSchema.statics.register = function (classid, callback) {
  Class.findOne({ classid: classid })
    .exec(function (err, classOb) {
      if (err) {
        return callback(err)
      } else if (!classOb) {
        var err = new Error('Class not found.');
        err.status = 401;
        return callback(err);
      }
      if (classOb.allowregister === true) {
        return callback(null, classOb);
      } else {
        return callback();
      }
    });
}

var Class = mongoose.model('Class', ClassSchema);
module.exports = Class;
