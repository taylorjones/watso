var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var JobSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true,
    required: true
  },
  email: {
    type: String,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  stdin: {
    type: String,
    required: false,
    trim: true
  },
  cppstandard: {
    type: String,
    required: false,
    trim: true
  },
  optimization: {
    type: String,
    required: false,
    trim: true
  },
  processors: {
    type: Number,
    required: true
  },
  status: {
    type: Number,
    required: true
  }
});

var Job = mongoose.model('Job', JobSchema);
module.exports = Job;
