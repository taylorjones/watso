var Class = require("../models/class");
var User = require("../models/user");

module.exports = {
  loadClasses: (req, res, next) => {
    User.findById(req.session.userId).exec(function(error, user) {
      if(user.isAdmin){
        Class.find().exec(function(error, classes) {
          if (error) {
            return next(error);
          } else {
            if (classes === null) {
              var err = new Error("Classes not found");
              err.status = 400;
              return next(err);
            } else {
              return res.render("classes", { classes: classes });
            }
          }
        });
      } else {
        return res.redirect("/");
      }
    });
  },

  newClass: (req, res, next) => {
    if (req.fields.classname && req.fields.classid) {
      var classData = {
        classname: req.fields.classname,
        classid: req.fields.classid,
        allowregister: true
      };
      Class.create(classData, function(error, user) {
        if (error) {
          return next(error);
        } else {
          return res.redirect("/classes");
        }
      });
    }
  }
}
