var express = require("express");
var path = require("path");
var zip = require("express-easy-zip");
var router = express.Router();
var { newJob } = require("./job");
var { loadClasses, newClass } = require("./class");
var {
  home,
  loadUser,
  profile,
  jobInfo,
  reZip,
  login,
  adminLogin,
  logout
} = require("./user");
var User = require("../models/user");
var Class = require("../models/class");
var Job = require("../models/job");

router.use(loadUser);
router.use(zip());

// GET route for reading data
router.get("/", home);

router.get("/classes", loadClasses);
router.post("/classes", newClass);

router.post("/newjob", newJob);

//POST route for updating data
router.post("/", login);
router.post("/admin", adminLogin);
router.get("/profile", profile);

//GET route for getting submitted job description
router.get("/job/:jobID", jobInfo);
//GET route for zipping and downloading submitted files
router.get("/zip/:jobID", reZip);
router.get("/logout", logout);

module.exports = router;
