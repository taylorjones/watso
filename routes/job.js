var fs = require("file-system");
var Job = require("../models/job");
var User = require("../models/user");
var Class = require("../models/class");

var curJob = -1;

Job.findOne()
  .sort("-id")
  .exec(function(err, member) {
    if (member === null) {
      curJob = 0;
    } else {
      curJob = member.id;
    }
  });

function build(dir, std, opt) {
  var execSync = require("child_process").execSync;
  try {
    execSync(`scripts/build.sh ${dir} ${String(std)} ${String(opt)}`);
  } catch (err) {
    if (err) return err.stderr;
  }
  return 0;
}

module.exports = {
  newJob: (req, res, next) => {
    console.log("New Job");
    User.findById(req.session.userId).exec(function(error, user) {
      if (error) {
        return next(error);
      }
      if (user === null) {
        return res.redirect("/");
      }
      Class.findOne({ classid: user.classid }).exec(function(error, classob) {
        if (error) {
          return next(error);
        } else {
          if (!user.isAdmin && (classob === null || classob.allowregister !== true)) {
            console.log("Job not allowed for classid "+user.classid);
            console.log(classob);
            return next("You are not allowed to create jobs.");
          } else {
            let jobId = ++curJob;
            console.log(jobId);
            console.log(req.files.project.type);
            dir = "jobs/" + jobId;
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            var execSync = require("child_process").execSync;
            switch (req.files.project.type) {
              case "application/x-compressed":
              case "application/x-tar":
              case "application/gzip":
                var tar = require("tar");
                tar.extract({
                  file: req.files.project.path,
                  sync: true,
                  cwd: dir
                });
                break;
              case "application/x-bzip":
                // This isn't portable, but it gets the job done for now
                execSync(`tar xjf ${req.files.project.path} -C ${dir}`);
                break;
              case "application/x-zip-compressed":
              case "application/zip":
                // This isn't portable, but it gets the job done for now
                execSync(`unzip ${req.files.project.path} -d ${dir}`);
                break;
              case "text/plain":
              case "application/octet-stream":
              case "text/x-c++src":
              default:
                fs.copyFileSync(
                  req.files.project.path,
                  dir + "/" + req.files.project.name
                );
                break;
            }
            console.log("Building");
            build(dir, req.fields.standard, req.fields.optimize);
            var jobData = {
              id: curJob,
              name: req.fields.jobname,
              email: user.email,
              stdin: req.fields.stdin,
              processors: req.fields.numprocessors,
              cppstandard: req.fields.standard,
              optimization: req.fields.optimize,
              status: 0,
            };
            Job.create(jobData, function(error, job) {
              return res.redirect("/profile");
            });
          }
        }
      });
    });
  }
}
