var User = require("../models/user");
var Class = require("../models/class");
var Job = require("../models/job");

module.exports = {
  home: function(req, res, next) {
    if (req.session.userId) {
      User.findById(req.session.userId).exec(function(error, user) {
        if (error) {
          return res.next(error);
        } else {
          if (user === null) {
            req.session.userId = false;
            return res.redirect("/");
          } else {
            return res.redirect("/profile");
          }
        }
      });
    } else {
      User.count().exec(function(error, count){
        if (count == 0) {
          return res.render("admin");
        }
        else {
          return res.render("home");
        }
      });

    }
  },

  loadUser: (req, res, next) => {
    if (req.session.userId) {
      User.findById(req.session.userId).exec(function(error, user) {
        if (error) {
          next();
        } else {
          if (user === null) {
            res.locals.user = false;
            next();
          } else {
            res.locals.user = req.user;
            next();
          }
        }
      });
    } else {
      res.locals.user = false;
      next();
    }
  },

  profile: (req, res, next) => {
    User.findById(req.session.userId).exec(function(error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          return res.redirect("/");
        } else {
          Job.find({ email: user.email }).exec(function(error, jobs) {
            if (error) console.error(error);
            return res.render("profile", { user: user, jobs: jobs });
          });
        }
      }
    });
  },

  jobInfo: (req, res, next) => {
    User.findById(req.session.userId).exec(function(error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          return res.redirect("/");
        } else {
          Job.find({ email: user.email, id: req.params.jobID }).exec(function(
            error,
            job
          ) {
            if (error) {
              return next(error);
            } else {
              if (job === null) {
                // This user doesn't have access to this job or the job doesn't exist
                return res.redirect("/profile");
              } else {
                console.log(job);
                return res.render("job", { user: user, job: job });
              }
            }
          });
        }
      }
    });
  },

  reZip: (req, res, next) => {
    User.findById(req.session.userId).exec(function(error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          return res.redirect("/");
        } else {
          Job.find({ email: user.email, id: req.params.jobID }).exec(function(
            error,
            job
          ) {
            if (error) {
              return next(error);
            } else {
              if (job === null) {
                // This user doesn't have access to this job or the job doesn't exist
                return res.redirect("/profile");
              } else {
                var dir = "jobs/" + req.params.jobID + "/";
                res.zip({
                  files: [{ path: dir, name: req.params.jobID }],
                  filename: "submitted-files.zip"
                });
              }
            }
          });
        }
      }
    });
  },

  login: (req, res, next) => {
    // confirm that user typed same password twice
    if (req.fields.password !== req.fields.passwordConf) {
      var err = new Error("Passwords do not match.");
      err.status = 400;
      return next(err);
    }

    if (
      req.fields.email &&
      req.fields.displayname &&
      req.fields.classid &&
      req.fields.password &&
      req.fields.passwordConf
    ) {
      Class.register(req.fields.classid, function(error, classOb) {
        if (error || !classOb) {
          var err = new Error("Invalid classid.");
          err.status = 401;
          return next(err);
        } else {
          var userData = {
            email: req.fields.email,
            displayname: req.fields.displayname,
            classid: req.fields.classid,
            password: req.fields.password,
            passwordConf: req.fields.passwordConf,
            isAdmin: false,
          };
          User.create(userData, function(error, user) {
            if (error) {
              return next(error);
            } else {
              req.session.userId = user._id;
              return res.redirect("/profile");
            }
          });
        }
      });
    } else if (req.fields.logemail && req.fields.logpassword) {
      User.authenticate(req.fields.logemail, req.fields.logpassword, function(
        error,
        user
      ) {
        if (error || !user) {
          var err = new Error("Wrong email or password.");
          err.status = 401;
          return next(err);
        } else {
          req.session.userId = user._id;
          return res.redirect("/profile");
        }
      });
    } else {
      var err = new Error("All fields required.");
      err.status = 400;
      return next(err);
    }
  },

  adminLogin: (req, res, next) => {
    // confirm that user typed same password twice
    User.count().exec(function(error, count){
      if (count == 0) {
        if (req.fields.password !== req.fields.passwordConf) {
          var err = new Error("Passwords do not match.");
          err.status = 400;
          return next(err);
        }
        if (
          req.fields.email &&
          req.fields.displayname &&
          req.fields.password &&
          req.fields.passwordConf
        ) {
          var userData = {
            email: req.fields.email,
            displayname: req.fields.displayname,
            password: req.fields.password,
            passwordConf: req.fields.passwordConf,
            isAdmin: true
          };
          User.create(userData, function(error, user) {
            if (error) {
              return next(error);
            } else {
              req.session.userId = user._id;
              return res.redirect("/classes");
            }
          });
        } else if (req.fields.logemail && req.fields.logpassword) {
          User.authenticate(req.fields.logemail, req.fields.logpassword, function(
            error,
            user
          ) {
            if (error || !user) {
              var err = new Error("Wrong email or password.");
              err.status = 401;
              return next(err);
            } else {
              req.session.userId = user._id;
              return res.redirect("/profile");
            }
          });
        } else {
          var err = new Error("All fields required.");
          err.status = 400;
          return next(err);
        }
      }
      else {
        return res.redirect("/");
      }
    });
  },

  logout: (req, res, next) => {
    if (req.session) {
      // delete session object
      req.session.destroy(function(err) {
        if (err) {
          return next(err);
        } else {
          return res.redirect("/");
        }
      });
    }
  }
};
