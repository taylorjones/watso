var express = require('express');
var exphbs  = require('express-handlebars');
var app = express();
var formidableMiddleware = require('express-formidable');
var mongoose = require('mongoose');
var session = require('express-session');
var { spawn } = require('child_process')
var MongoStore = require('connect-mongo')(session);
mongoose.Promise = require('bluebird');
//connect to MongoDB
mongoose.connect('mongodb://localhost/watso', {
  useMongoClient: true,
  /* other options */
});
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
});

// kick off python worker for running jobs
let childproc = spawn('python', ['scripts/worker.py'], {
  stdio: [0, 1, 2]
}).unref();
console.log("spawned python worker")

var cleanExit = function() { childproc.exit() };
process.on('SIGINT', cleanExit); // catch ctrl-c
process.on('SIGTERM', cleanExit); // catch kill

try {

//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
app.use(formidableMiddleware());


// serve static files from template
app.use(express.static(__dirname + '/public'));
app.engine('handlebars', exphbs({defaultLayout: 'main',
helpers:{
  // Function to do basic mathematical operation in handlebar
  math: function(lvalue, operator, rvalue) {lvalue = parseFloat(lvalue);
      rvalue = parseFloat(rvalue);
      return {
          "+": lvalue + rvalue,
          "-": lvalue - rvalue,
          "*": lvalue * rvalue,
          "/": lvalue / rvalue,
          "%": lvalue % rvalue
      }[operator];
  }}
}));
app.set('view engine', 'handlebars');

// include routes
var routes = require('./routes/router');
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});


// listen on port 3000
app.listen(8080, function () {
  console.log('Express app listening on port 8080');
});

} catch(error) {
  console.log(error);
  cleanExit();
}


