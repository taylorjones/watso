#!/bin/bash

set -x

if [[ $# -lt 1 ]]; then
    echo -e "Provide a file to copy"
    exit 1
fi

for i in {0..15}; do 
    scp $1 "watso-$i.bluezone.usu.edu:"
done

