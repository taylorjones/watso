#!/bin/bash

# Arguments:
# $1 : directory in which to compile the job
# $2 : c++ standard to be used in compilation, empty string if none
# $3 : any optimization flags, empty string if none

CLEANEXIT=0
MKDIRERR=1
PUSHPOPERR=2
COMPILE_ERR=3

pushd "$1" > /dev/null || { echo $PUSHPOPERR && exit $PUSHPOPERR; }
mpic++ *.cpp $2 $3 > "./compiler.output" 2>&1 || { echo $COMPILE_ERR && exit $COMPILE_ERR; }
popd > /dev/null || { echo $PUSHPOPERR && exit $PUSHPOPERR; }
echo $CLEANEXIT
exit $CLEANEXIT
