#!/bin/bash

# Arguments:
# $1 : jobid for the job that failed to compile

{
  rm jobs/"$1"/build/*;
  rmdir jobs/"$1"/build/;
  rm jobs/"$1"/*;
  rmdir jobs/"$1";
} > /dev/null 2>&1