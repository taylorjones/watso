#!/usr/bin/python

if __name__ == "__main__":
    import pymongo
    import os
    from time import sleep
    from subprocess import call

    def update_status(id):
        db.jobs.update_one(
            {'id': id},
            {'$inc': {'status': 1}},
            upsert=False
        )

    def reset_jobs(collection):
        """Resets all jobs for debugging purposes"""
        collection.update_many(
            {},
            {'$set': {'status': 0}},
            upsert=False
        )

    client = pymongo.MongoClient()
    db = client.watso

    #reset_jobs(db.jobs)

    while True:

        print 'looking for jobs...'
        doc = db.jobs.find_one({ 'status': 0 }, sort=[("id", pymongo.ASCENDING)])
        if doc != None:
            id = doc['id']
            print 'found job %d' % id
            update_status(id)
            if doc['stdin'] != '':
                call(['scripts/run.sh', str(id), str(doc['processors']), str(doc['email']), doc['stdin']])
            else:
                call(['scripts/run.sh', str(id), str(doc['processors']), str(doc['email'])])
            update_status(id)

        else:
            # Constant queries are expensive
            # Sleep for awhile if there are no new jobs to run
            sleep(5)
