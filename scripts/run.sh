#!/bin/bash

# Arguments:
# $1 : jobid for the job to run
# $2 : number of processes to run with
# $3 : user email address
# $4 : file to be read as STDIN, if there is one

TIMEOUT=60

pushd "jobs/$1" > /dev/null || exit 1
if [ $# -gt 3 ]; then

  # Process stdin file
  cmdargs=''
  while read -r line; do
    cmdargs+=" $line"
  done < "$4"

  ( mpirun -np "$2" --hostfile "/home/pi/watso/scripts/hostfile" ./a.out "$cmdargs" > "output.log" 2>&1 ) & pid=$!
  ( sleep $TIMEOUT && kill -HUP $pid ) 2>/dev/null & watcher=$!
  if wait $pid 2>/dev/null; then
    pkill -HUP -P $watcher
	  wait $watcher
  else
    ( echo -e "!***** Job timed out *****!\n"; cat "output.log" ) > tmpfile; mv tmpfile "output.log"
  fi

else
  # No stdin file provided
  ( mpirun -np "$2" --hostfile "/home/pi/watso/scripts/hostfile" ./a.out > "output.log" 2>&1 ) & pid=$!
  ( sleep $TIMEOUT && kill -HUP $pid ) 2>/dev/null & watcher=$!
  if wait $pid 2>/dev/null; then
    pkill -HUP -P $watcher
	  wait $watcher
  else
    ( echo -e "!***** Job timed out *****!\n"; cat "output.log" ) > tmpfile; mv tmpfile "output.log"
  fi

fi

echo -e "To: $3\nFrom: piclusterwatso@gmail.com\nSubject: Your WATSO Job is Complete\n\nThe MPI job you submitted to the WATSO pi cluster has completed. You can download your results by visiting watso.bluezone.usu.edu/job/$1/ and clicking the download button." | msmtp "$3"

popd > /dev/null || exit 1
