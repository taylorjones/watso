#!/bin/bash

set -x

for i in {0..9}; do 
    ssh-copy-id "192.168.0.10$i"
done

for i in {10..15}; do 
    ssh-copy-id "192.168.0.1$i"
done

